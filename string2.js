const ExtractNumsFromIpAddress = (ipAddress) => {
    const newArr = ipAddress.split(".");
    
    const isNumArray = newArr.every((currentValue, index, newArray) => parseInt(currentValue))
    // console.log(isNumArray)
    if (isNumArray) {
        return newArr.map(num => parseInt(num))
    }
    return [];
}


module.exports =  { ExtractNumsFromIpAddress } ;
