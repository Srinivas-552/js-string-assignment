const ExtractMonthFromDate = (inputDate) => {
    const date = new Date(inputDate); //const d = new Date("03/25/2015"); JavaScript Short Dates.
    const month = date.getMonth() + 1;
    return month;
}


module.exports =  { ExtractMonthFromDate } ;