/* 
    Create a function for each problem in a file called
        string1.js
        string2.js
        string3.js
    and so on in the root of the project.
    
    Ensure that the functions in each file is exported and tested in its own file called
        testString1.js
        testString2.js
        testString3.js
    and so on in a folder called test.

    Each function must take at least one argument. 
	
	Example:
		const result = string1("test string");

	If you want to pass more arguments, that is up to you.

	Create a new git repo on gitlab for this project, ensure that you commit after you complete each problem in the project. 
	
    Ensure that the repo is a public repo.
	
	When you are done, send the gitlab url to your mentor.

    Try to use the inbuilt javascript string methods as much as possible. 

*/

// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 
parseInt("-10") + "<br>" +
  parseInt("-10.33") + "<br>" +
  parseInt("10") + "<br>" +
  parseInt("10.33") + "<br>" +
  parseInt("10 6") + "<br>" +  
  parseInt("10 years") + "<br>" +  //10
  parseInt("years 10"); //Nan
  ******************************************
  parseFloat("10") + "<br>" +
  parseFloat("10.33") + "<br>" +
  parseFloat("10 6") + "<br>" +  	//10
  parseFloat("10 years") + "<br>" +	//10
  parseFloat("years 10"); //Nan


// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
let text = "Hello";
const myArr = text.split("");

text = "";
for (let i = 0; i < myArr.length; i++) {
  text += myArr[i] + "<br>"
}

// Support IPV4 addresses only. If there are other characters detected, return an empty array.


// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.
const d = new Date("03/25/2015");
Use the correct Date method to get the month (0-11) out of a date object.

const d = new Date();
month = d.getMonth();

// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}
const person = {fname:"John", lname:"Doe", age:25}; 

let txt = "";
for (let x in person) {
  txt += person[x] + " ";
}

// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.
const cars = ["BMW", "Volvo", "Mini"];

let text = "";
for (let x of cars) {
  text += x + "<br>";
}
***************************
Syntax:
string.concat(string1, string2, ..., stringX)
