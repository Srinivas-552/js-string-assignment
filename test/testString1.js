
const string1 = require("../string1");

// const strArr = ["$100.45", "$1,002.22", "-$123", "$120,,0"]

console.log(string1.convertStringToNum("$100.45"));

console.log(string1.convertStringToNum("$1,002.22"));

console.log(string1.convertStringToNum("-$123"));

console.log(string1.convertStringToNum("$abc"));

console.log(string1.convertStringToNum("$120,,0"));