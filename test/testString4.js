const string4 = require("../string4");

let inputObj = {"first_name": "JoHN", "last_name": "SMith"};

const fullName = string4.mergeToFullName(inputObj);

console.log(fullName);

// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}